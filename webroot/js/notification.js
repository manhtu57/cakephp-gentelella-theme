function createSidebarNotification(push_data) {
  if (push_data) {
    $(push_data).each(function(index, element) {
      buildNotification(element.id, element.title, element.message, element.date);
    })
  }
}

function buildNotification(id, title, message, date) {
  if ($('.event_' + id).length === 0) {
    date_timeago = moment(date).lang("it").fromNow();

    var container = $('#div_notification')[0];
    var firstChild = container.children[0];

    var toast__content = el("div.toast__content", [
      el("div.toast__title", [
        el("p.toast__title_text", title),
        el("p.toast__title_timeago pointer", date_timeago, {
          'data-toggle': 'tooltip',
          'title': moment(date).lang("it").format('dddd d MMMM YYYY h:mm:ss')
        })
      ]),
      el("p.toast__message", $.parseHTML(message))
    ]);

    var toast__notification = el("div.toast__notification event_" + id, [
      el('div.toast__icon', el('i.fas fa-info')),
      toast__content,
      el("i.fas fa-times toast__close", {
        'data-event-id': id
      })
    ]);

    mount(container, toast__notification, firstChild);
    updateBadgeCounter(+1);

    $('.toast__notification').mouseenter(function(){
      $(this).find('.toast__close').show();
    })
    .mouseleave(function(){
      $(this).find('.toast__close').hide();
    });
  }
}

$(document).on('click',".toast__close",function(element) {
  close_button = $(element.currentTarget);
  event_id = close_button.data('event-id');
  $('.event_' + event_id).fadeOut(300, function() {
    $('.event_' + event_id).remove();
  });

  $.get("/Utility/pushNotificationRead/" + event_id, function(data) {
    noty_obj = null
  });
  updateBadgeCounter(-1);
});

function initBadgeCounter(new_value){
  $('#quick_panel_badge').text(new_value);
}

function updateBadgeCounter(new_value){
  badge_val = $('#quick_panel_badge').text();
  badge_val = parseInt(badge_val);
  var new_value = badge_val + new_value;
  $('#quick_panel_badge').text(new_value);
  
  animateCSS('#quick_panel_badge','shake');
}