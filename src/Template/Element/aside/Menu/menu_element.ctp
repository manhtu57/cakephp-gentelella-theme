<span>
  <?php
  $icon = "<i class='{$element['font_awesome']}'></i>";
  $text = "<span class='menu_text'>" . ucfirst(__($element['name'])) . "</span>";

  $pinned = null;
  if (!empty($menu_icon_pinned)) {
    $pinned = in_array($element['id'], array_keys($menu_icon_pinned));
  }
  ?>
  <?= $this->Html->link($icon . $text, [
    'controller' => $element['controller'],
    'action' => $element['action']
  ], ['escape' => false]) ?>
  <i title="<?= __("Aggiungi scorciatoia") ?>" class="fas fa-thumbtack pointer menu-pin <?= ($pinned) ? 'green' : '' ?>" data-menu-id="<?= $element['id'] ?>"></i>
</span>