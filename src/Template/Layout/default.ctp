<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <?php $title = isset($title) ? $title : h($this->fetch('title')); ?>
  <title><?= $title ?></title>
  <?= $this->Html->meta(
    'favicon.ico',
    '/webroot/img/nolo_icon/favicon.png',
    ['type' => 'icon']
  );
  ?>

  <?= $this->Html->script("/webroot/gentelella_node_modules/mmenu-js/dist/mmenu") ?>
  <?= $this->Html->css("/webroot/gentelella_node_modules/mmenu-js/dist/mmenu") ?>

  <?= $this->Html->script("/webroot/gentelella_node_modules/mmenu-js/dist/wrappers/bootstrap/mmenu.bootstrap") ?>
  <?= $this->Html->css("/webroot/gentelella_node_modules/mmenu-js/dist/wrappers/bootstrap/mmenu.bootstrap") ?>

  <?= $this->Html->css("/webroot/gentelella_node_modules/mmenu-js/dist/addons/sidebar/mmenu.sidebar") ?>
  <?= $this->Html->css("/webroot/gentelella_node_modules/mmenu-js/dist/addons/navbars/mmenu.navbars") ?>

  <?= $this->Html->css("/webroot/gentelella_node_modules/mburger-css/dist/mburger") ?>

  <?= $this->Html->css('/webroot/gentelella_vendor/components/font-awesome/css/all'); ?>

  <?= $this->Html->script('/webroot/gentelella_vendor/components/jquery/jquery.min'); ?>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <?= $this->Html->script('/webroot/gentelella_vendor/moment/moment/min/moment-with-locales.min'); ?>

  <!-- BOOTSTRAP COMPOSER -->
  <?= $this->Html->css('/webroot/gentelella_vendor/twbs/bootstrap/dist/css/bootstrap') ?>
  <?= $this->Html->script('/webroot/gentelella_vendor/twbs/bootstrap/dist/js/bootstrap.bundle') ?>

  <?php $this->Html->script('/webroot/gentelella_vendor/edsol/bootstrap-fileupload/dist/bootstrap-FileUpload.min'); ?>
  <?php $this->Html->css('/webroot/gentelella_vendor/edsol/bootstrap-fileupload/dist/bootstrap-FileUpload.min'); ?>

  <?= $this->Html->script('Plugins/jquery-loading-overlay/dist/loadingoverlay.min') ?>
  <?= $this->Html->script('Plugins/jquery.redirect') ?>
  <!-- NOTY PLUGIN -->
  <?= $this->Html->script('/webroot/gentelella_vendor/needim/noty/lib/noty'); ?>
  <?= $this->Html->css('/webroot/gentelella_vendor/needim/noty/lib/noty'); ?>

  <?= $this->Html->script("/webroot/gentelella_vendor/select2/select2/dist/js/select2.full.min") ?>
  <?= $this->Html->css("/webroot/gentelella_vendor/select2/select2/dist/css/select2.min") ?>

  <?= $this->Html->css('/webroot/gentelella/plugins/select2-bootstrap4-theme/dist/select2-bootstrap4.min') ?>
  <?= $this->Html->script("select-togglebutton") ?>

  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

  <?php $this->Html->css('/webroot/gentelella_vendor/bootstrap-glyphicons/bootstrap4-glyphicons/css/bootstrap-glyphicons.min'); ?>

  <?= $this->Html->css('/webroot/gentelella_node_modules/animate.css/animate.compat'); ?>

  <?= $this->Html->script('/webroot/gentelella/plugins/jquery-confirm/dist/jquery-confirm.min') ?>
  <?= $this->Html->css('/webroot/gentelella/plugins/jquery-confirm/dist/jquery-confirm.min') ?>

  <?= $this->Html->script('Gentelella./build/js/jquery.sse.min') ?>

  <?= $this->Html->script("/webroot/gentelella_node_modules/bootbox/dist/bootbox.all.min") ?>

  <!-- CUSTOM EDOO -->
  <?= $this->Html->css('Gentelella./css/edoo-custom') ?>
  <?= $this->Html->script('Gentelella./js/edoo-custom') ?>
  <?= $this->Html->script('Gentelella./js/Utils') ?>
  <?= $this->Html->script('Gentelella./js/init') ?>

  <!--CSS PRIMARY WEBROOT-->
  <?= $this->Html->css('/css/default'); ?>
  <?= $this->Html->script('default'); ?>
  <?= $this->Html->script('modernizr'); ?>

  <!-- CHECKBOX CUSTOM PLUGIN-->
  <?= $this->Html->script('/webroot/gentelella/plugins/Checkbox2Button/js/checkbox2button'); ?>
  <?= $this->Html->css('/webroot/gentelella/plugins/Checkbox2Button/css/checkbox2button'); ?>

  <!-- FILE INPUT PLUGIN -->
  <?php $this->Html->script('/webroot/gentelella/plugins/bootstrap-fileinput/js/fileinput.min'); ?>
  <?php $this->Html->css('/webroot/gentelella/plugins/bootstrap-fileinput/css/fileinput.min'); ?>

  <?= $this->Html->script('/webroot/gentelella/plugins/bootstrap4-datetimepicker/build/js/bootstrap-datetimepicker.min'); ?>
  <?= $this->Html->css('/webroot/gentelella/plugins/bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.min'); ?>

  <?= $this->Html->script('/webroot/gentelella_node_modules/redom/dist/redom.min') ?>
  <script>
    const {
      el,
      mount
    } = redom;
  </script>
  <?= $this->Html->script('Gentelella./js/Plugins/jquery.slidereveal.min') ?>

  <?php echo $this->fetch('css'); ?>
</head>

<body>
  <div class="loader"></div>
  <?php
  if (!empty($user)) :
    echo $this->element('aside/Navtop/navbar');
  endif;
  ?>
  <?= $this->element('aside/SidebarNotification/sidebar') ?>
  <?php
  if (!empty($user)) :
    echo $this->element('aside/Menu/menu', [
      'theme' => (!empty($user['preferenze']['theme'])) ? $user['preferenze']['theme'] : 'light'
    ]);
  endif;
  ?>
  <div id="my-wrapper">
    <div id="my-header"></div>
    <div id="my-content">
      <div class="main_container">
        <?= $this->element("aside/breadcrumb") ?>
        <?= $this->Flash->render(); ?>
        <?= $this->Flash->render('auth'); ?>
        <?= $this->fetch('content'); ?>
      </div>
    </div>
    <div id="my-footer">
      <?= $this->fetch('script'); ?>
      <?= $this->fetch('scriptBottom'); ?>
      <a id="toTop" class="hide"><i class="fas fa-arrow-up"></i></a>
    </div>
  </div>
</body>

</html>