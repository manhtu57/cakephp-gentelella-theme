<div class="btn-group">

  <button class="btn btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <span class="profile-img"><?= $this->Html->image($user['full_path_immagine'],['class'=>'']) ?></span>
    <span class="profile-name"><?= ucwords($user['username']) ?></span>
  </button>

  <div class="dropdown-menu dropdown-menu-right">
    <a class="dropdown-item" href="#"><i class="far fa-user-circle"></i> <?= __('Profilo') ?></a>
    <a class="dropdown-item" href="#"><i class="fas fa-cog"></i> <?= __('Impostazioni') ?></a>
    <a class="dropdown-item" href="<?= $this->Url->build(['controller'=>'Users','action'=>'logout']) ?>"><i class="fas fa-sign-in-alt"></i> <?= __('Esci') ?></a>
  </div>
  
</div>