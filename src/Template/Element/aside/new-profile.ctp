<ul class="navbar-nav ml-auto nav-flex-icons">
  <li class="nav-item avatar">
    <a class="nav-link p-0" href="#">
      <span class="profile-img">
        <?= $this->Html->image($user['full_path_immagine'],['class'=>'rounded-circle z-depth-0']) ?>
      </span>
    </a>
  </li>
</ul>
