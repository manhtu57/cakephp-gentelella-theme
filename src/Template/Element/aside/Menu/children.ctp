<?php foreach ($children as $element) :
  $children_can_access = null;
  $menu_role_permission = $element['role_permission'];
  // debug($user_permission);exit;
  foreach ($user_permission as $role) {
    // debug($menu_role_permission[$role]);
    if ($menu_role_permission[$role] == 'true') {
      // debug("HA ACCESSO");
      $children_can_access = true;
    } else {
      // debug("NON HA ACCESSO");
      if (!$children_can_access) {
        $children_can_access = false;
      }
    }
  }
  if (empty($children_can_access)) {
    $children_can_access = false;
  }
?>
  <?php if ($children_can_access) : ?>
    <li>
      <a href="<?= $this->Url->build(['controller' => $element['controller'], 'action' => $element['action']]) ?>">
        <span class="fa-stack fa-lg pull-left">
          <i class="<?= $element['font_awesome'] ?> fa-stack-1x"></i>
        </span>
        <span class="menu-items"><?= ucfirst(__d('menu', $element['name'])) ?></span>
      </a>
    </li>
  <?php endif ?>
<?php endforeach ?>