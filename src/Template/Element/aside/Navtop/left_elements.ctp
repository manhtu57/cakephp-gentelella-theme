<div class="left-elements">
  <?php
  $element = "{$this->templatePath}/{$this->template}/toolbar";

  if (!$this->elementExists($element)) {
    $element = "{$this->templatePath}/" . ucfirst($this->template) . "/toolbar";
  }
  if (!$this->elementExists($element)) {
    $element = "{$this->templatePath}/" . Cake\Utility\Inflector::camelize($this->template) . "/toolbar";
  }

  if ($this->elementExists($element)) {
    echo $this->element($element);
  }
  ?>
</div>