<?php
use Cake\Core\Configure;
use Cake\Core\Exception\MissingPluginException;
use Cake\Core\Plugin;
use Cake\Cache\Cache;

Configure::write('Theme', [
    'title' => 'NoloBI'
  ]);

Plugin::load('BootstrapUI');
Plugin::load('ADmad/Tree');