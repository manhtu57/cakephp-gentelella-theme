<aside class="right-sidebar">
  <div class="timeline-title">
    <span class="sidebar-title"><?= __('Attività') ?></span>
    <span class="sidebar-refresh-icon">
      <i class="fa fa-refresh"></i>
    </span>
  </div>
  <div class="timeline-body">
    <div id="loading" class="modal-ajax"></div>
    <ul class="timeline">
      <!--JQUERY POPOLATE-->
  </ul>
</div>
</aside>

<script>
$(document).ready(function(){
  $(document).ajaxStart(function(){
      $('#loading').show();
   }).ajaxStop(function(){
      $('#loading').hide();
   });
  $('.sidebar-refresh-icon').on("click", activityLoad);
  activityLoad();
});

function activityLoad(){
  $(".timeline").empty();
  $.ajax({
    type: 'POST',
    url: '/operazioni/getOperazioniJson/'+10,
    dataType: "json",
    cache : false,
      success: function(data){
        $.each(data,function(key,data){
          li='<li>'+
                '<p>'+
                  '<a href="">'+data.user+'</a> '+data.message+
                  ' <span class="timeline-icon"><i class="fa fa-file"></i></span>'+
                  '<span class="timeline-date">'+data.data+'</span>'+
                '</p>'+
              '</li>';
          $('.timeline').append(li);
        });
      },
      error: function (data){
        console.log(data);
      }
 })
}
</script>
