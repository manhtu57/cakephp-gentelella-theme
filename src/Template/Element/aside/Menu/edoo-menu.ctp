<div id="wrapper" class="toggled-2 d-print-none">

  <div id="sidebar-wrapper">
    <ul class="sidebar-nav nav-pills nav-stacked" id="menu">

      <?php $user_permission = array_keys($user['lista_ruoli']);
      foreach ($menu_list as $key => $menu) :
        $can_access = false;
        $menu_role_permission = $menu['role_permission'];
        foreach ($user_permission as $role) {
          // debug($menu_role_permission[$role]);
          if ($menu_role_permission[$role] == 'true') {
            $can_access = true;
          } else {
            if (!$can_access) {
              $can_access = false;
            }
          }
        }
        if ($can_access) :
          if (empty($menu['children'])) : ?>
            <li>
              <a href="<?= $this->Url->build(['controller' => $menu['controller'], 'action' => $menu['action']]) ?>">
                <span class="fa-stack fa-lg pull-left">
                  <i class="<?= $menu['font_awesome'] ?> fa-stack-1x"></i>
                </span> <?= ucfirst(__($menu['name'])) ?>
              </a>
            </li>

          <?php else : ?>

            <li class="active">
              <a href="<?= $this->Url->build(['controller' => $menu['controller'], 'action' => $menu['action']]) ?>">
                <span class="fa-stack fa-lg pull-left">
                  <i class="<?= $menu['font_awesome'] ?> fa-stack-1x"></i>
                </span> <?= ucfirst(__($menu['name'])) ?>
              </a>

              <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                <?= $this->element('aside/Menu/children', [
                  'user_permission' => $user_permission,
                  'children' => $menu['children']
                ]) ?>
              </ul>
            </li>

          <?php endif ?>
        <?php endif ?>
      <?php endforeach ?>
    </ul>
  </div>
</div>


<?= $this->Html->script("Gentelella./js/edoo-sidebar-menu"); ?>
<?= $this->Html->css('Gentelella./css/edoo-sidebar-menu'); ?>

<style>
  #sidebar-footer {
    width: 250px;
    bottom: 0px;
    background: red;
    position: fixed;
    height: 40px;
    background-color: red;
  }
</style>