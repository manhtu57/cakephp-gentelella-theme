<span>
  <?php foreach ($elements as $element) {
    $icon = "<i class='{$element['font_awesome']}'></i>";
    $text = "<span class='menu_text'>" . ucfirst(__($element['name'])) . "</span>";

    echo $this->Html->link($icon . $text, [
      'controller' => $element['controller'],
      'action' => $element['action']
    ], ['escape' => false]);
  } ?>
</span>